# Copyright 2018 Rasmus Thomsen <cogitri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gitlab [ prefix="https://gitlab.freedesktop.org" new_download_scheme=true ] \
    test-dbus-daemon \
    meson

export_exlib_phases src_test src_install

SUMMARY="Thunderbolt 3 device manager"

LICENCES="LGPL-2.1"
SLOT="0"
MYOPTIONS="
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        app-doc/asciidoc
        virtual/pkg-config
    build+run:
        dev-libs/glib:2[>=2.56]
        sys-auth/polkit:1[>=0.114]
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd )
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dcoverity=false
    -Ddb-name=boltd
    -Dinstall-tests=false
    -Dman=true
    -Dprivileged-group=wheel
    -Dprofiling=false
    -Dsystemd=false
)

bolt_src_test() {
    esandbox allow_net "unix:${TEMP%/}/bolt.unix.*/notify_socket"

    test-dbus-daemon_run-tests meson_src_test

    esandbox disallow_net "unix:${TEMP%/}/bolt.unix.*/notify_socket"
}

bolt_src_install() {
    meson_src_install

    keepdir /var/lib/boltd
}
