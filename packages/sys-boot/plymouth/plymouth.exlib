# Copyright 2010-2011 Brett Witherspoon <spoonb@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require systemd-service

export_exlib_phases src_configure src_install

SUMMARY="An application that provides graphical boot animation"
DESCRIPTION="
Plymouth is an application that runs very early in the boot process (even before the root filesystem
is mounted!) that provides a graphical boot animation while the boot process happens in the
background. It is designed to work on systems with DRM modesetting drivers. The idea is that early on in the
boot process the native mode for the computer is set, plymouth uses that mode, and that mode stays
throughout the entire boot process up to and after X starts. Ideally, the goal is to get rid of all
flicker during startup.
"
HOMEPAGE="https://freedesktop.org/wiki/Software/Plymouth"
DOWNLOADS="
    https://www.freedesktop.org/software/${PN}/releases/${PNV}.tar.xz
    https://dev.exherbo.org/distfiles/${PN}/${PN}-exherbo.tar.bz2
"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS="
    doc
    systemd [[ description = [ Systemd integration to coordinate boot up process ] ]]
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
        doc? ( dev-libs/libxslt )
    build+run:
        media-libs/libpng:=[>=1.2.16]
        x11-dri/libdrm
        x11-libs/pango[>=1.21.0]
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd )
        systemd? ( sys-apps/systemd )
    suggestion:
        sys-boot/dracut [[
            description = [ Provides mkinitrd functions and includes a plymouth module ]
        ]]
"

plymouth_src_configure() {
    econf \
        --localstatedir=/var                                                    \
        --disable-gtk                                                           \
        --disable-upstart-monitoring                                            \
        --enable-pango                                                          \
        --enable-tracing                                                        \
        --without-system-root-install                                           \
        --with-release-file=/etc/exherbo-release                                \
        --with-boot-tty=/dev/tty7                                               \
        --with-shutdown-tty=/dev/tty63                                          \
        --with-logo=/usr/share/plymouth/logo.png                                \
        --with-background-color=0x3391CD                                        \
        --with-background-start-color-stop=0x0073B3                             \
        --with-background-end-color-stop=0x00457E                               \
        --with-systemdunitdir="${SYSTEMDSYSTEMUNITDIR}"                         \
        --with-udev                                                             \
        --disable-more-warnings                                                 \
        --without-rhgb-compat-link                                              \
        $(option_enable doc documentation)                                      \
        $(option_enable systemd systemd-integration)
}

plymouth_src_install() {
    default

    insinto /usr/share/plymouth/
    doins "${WORKBASE}"/plymouth-exherbo/logo.png

    # `make install` creates this, but it's not needed
    edo rmdir "${IMAGE}"/var/run/{plymouth,}

    keepdir /var/spool/plymouth
    keepdir /var/lib/plymouth
}

