# Copyright © 2010 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ] gsettings
require vala [ vala_dep=true ]
require meson
require test-dbus-daemon

SUMMARY="People aggregation library"
DESCRIPTION="
libfolks is a library that aggregates people from multiple sources (eg,
Telepathy connection managers) to create metacontacts.
"
HOMEPAGE="https://wiki.gnome.org/Projects/Folks"

LICENCES="LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    bluetooth [[ description = [ Pull contacts from bluetooth devices ] ]]
    eds [[ description = [ Enable the evolution-data-server backend ] ]]
    import-tool [[ description = [ Enable the meta-contact import tool ] ]]
    inspect-tool [[ description = [ Enable the data inspection tool ] ]]
    telepathy [[ description = [ Telepathy IM provider ] ]]
"

DEPENDENCIES="
    build:
        dev-util/intltool[>=0.50.0]
        virtual/pkg-config[>=0.21]
    build+run:
        base/libgee:0.8[>=0.8.4][gobject-introspection]
        dev-libs/glib:2[>=2.44.0]
        gnome-desktop/gobject-introspection:1[>=1.30]
        bluetooth? ( gnome-desktop/evolution-data-server:1.2[>=3.33.2] )
        eds? (
            gnome-desktop/evolution-data-server:1.2[>=3.33.2][gobject-introspection][vapi]
            dev-libs/libxml2:2.0
        )
        import-tool? ( dev-libs/libxml2:2.0 )
        telepathy? (
            dev-libs/dbus-glib:1
            net-im/telepathy-glib[>=0.19.9][vapi]
        )
   test:
        bluetooth? ( dev-python/python-dbusmock )
"

# Some BluezBackend tests fail:
#
#  6/32 folks:BluezBackend / device-properties       FAIL            0.32s   killed by signal 5 SIGTRAP
#  7/32 folks:BluezBackend / individual-retrieval    FAIL            0.29s   killed by signal 5 SIGTRAP
#  8/32 folks:BluezBackend / vcard-parsing           FAIL            0.29s   killed by signal 5 SIGTRAP
RESTRICT="bluetooth? ( test )"

MESON_SRC_CONFIGURE_PARAMS=(
    -Ddocs=false
    -Dinstalled_tests=false
    -Dofono_backend=false
    -Dprofiling=false
    -Dzeitgeist=false
)
MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'bluetooth bluez_backend'
    'eds eds_backend'
    'import-tool import_tool'
    'inspect-tool inspect_tool'
    'telepathy telepathy_backend'
)
MESON_SRC_CONFIGURE_TESTS=(
    '-Dtests=true -Dtests=false'
)

pkg_setup() {
    meson_pkg_setup
    vala_pkg_setup
}

src_prepare() {
    meson_src_prepare

    # Use prefixed pkg-config
    edo sed -e "s/pkg-config/${PKG_CONFIG}/g" \
            -i tests/lib/eds/test-case.vala
}

src_test() {
    # EDS backend tests seem to interfere with each other causing one or the other to hit the
    # timeout limit
    export MESON_TESTTHREADS=1

    test-dbus-daemon_run-tests meson_src_test
}

